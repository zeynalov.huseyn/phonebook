﻿using PhoneBook_LDAP.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PhoneBook_LDAP.Controllers
{
    public class AdminController : Controller
    {
        public const int _rowPerPage = 50;

        // GET: Admin
        public ActionResult Index(string SeacrhFor = "", int Page = 1, int rowPerPage = _rowPerPage)
        {
            /*LDAPContext ld = new LDAPContext();

            IOrderedQueryable<PhoneBook_LDAP.Models.Users> users = from usr in ld.Users
                        orderby usr.DisplayName, usr.Phone
                        select usr;

            return View(users);*/

            LDAPContext ld = new LDAPContext();

            IQueryable<Users> usrs = ld.GetUserByNameAndPhone(SeacrhFor, Page, _rowPerPage);

            //Pagination-da aktiv page-i gostermek ucun istifade edecem
            ViewBag.Page = Page;

            return View(usrs);
        }


        //PB-daki deywikliyi save edir
        public JsonResult SaveUserData(PhoneBook_LDAP.Models.Users model)
        {
            string message = "NS";

            LDAPContext db = new LDAPContext();

            PhoneBook_LDAP.Models.Users UntToUpdate = db.Users
              .Where(p => p.UserName == model.UserName).FirstOrDefault();

            if (UntToUpdate != null)
            {
                UntToUpdate.DisplayName = model.DisplayName;

                db.SaveChanges();

                message = "Success";
            }

            //ViewBag.SavingSuccess = true;

            return Json(message);
        }


    }
}