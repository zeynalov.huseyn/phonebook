﻿using IntranetMvcApplication.Util;
using PhoneBook_LDAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PhoneBook_LDAP.Controllers
{
    [Authorize(Roles = "Programmers,Kadrlar sobesi")]
    public class PhoneBookController : Controller
    {

        public const int _rowPerPage = 50;

        // GET: PhoneBook
        public ActionResult Index(string SeacrhFor = "", int Page = 1, int rowPerPage = _rowPerPage)
        {
            LDAPContext ld = new LDAPContext();

            IQueryable<Users> usrs = ld.GetUserByNameAndPhone(SeacrhFor, Page, _rowPerPage);

            //Pagination-da aktiv page-i gostermek ucun istifade edecem
            ViewBag.Page = Page;

            return View(usrs);
            //return View(GetUsersFromLDAP(SeacrhFor, Page, _rowPerPage));
        }


        private IQueryable<Users> GetUsersFromLDAP(string SeacrhFor = "*", int Page = 1, int rowPerPage = _rowPerPage)
        {
            
            if(SeacrhFor == null || SeacrhFor == string.Empty)
                SeacrhFor = "*";
            else if (SeacrhFor != "*")
                SeacrhFor = "*" + SeacrhFor + "*";


            //Pagination-da aktiv page-i gostermek ucun istifade edecem
            ViewBag.Page = Page;

            LDAP_Tool alo = new LDAP_Tool();

            List<Users> usrs = alo.GetADUsers_ByNameOrPhone(SeacrhFor);

            IQueryable<Users> usrs_ = Enumerable.Empty<Users>().AsQueryable();
            
            try {
                usrs_ = usrs.AsQueryable();

                usrs_ = usrs_.Skip(rowPerPage * (Page - 1)).Take(rowPerPage);
            }
            catch (ArgumentNullException)
            {
                
            }

            return usrs_;
        }


    }
}