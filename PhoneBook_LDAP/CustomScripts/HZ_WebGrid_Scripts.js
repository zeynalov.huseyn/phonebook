﻿//GridView emeliyyatlari
$('.edit-mode').hide();
$('.edit-change, .cancel-change').on('click', function () {
    var tr = $(this).parents('tr:first');
    tr.find('.edit-mode, .display-mode').toggle();
});


//WebGrid save toggle
$('.save-change-Phones').on('click', function () {
    var tr = $(this).parents('tr:first');

    //var GUID = tr.find("#lblID").html();
    var UserName = tr.find("#lblUserName").val();
    var DisplayName = tr.find("#DisplayName").val();  //edit-mode Yeni melumati goturur
    /*var WorkDescription = tr.find("#WorkDescription").val();
    var Number = tr.find("#Number").val();
    var Department = tr.find("#Department").val();
    var txtDepartment = tr.find("#Department").find('option:selected').text();*/

    

    tr.find("#lblDisplayName").text(DisplayName); //display-mode-daki kohne melumati edit-mode-daki yeni melumatla evez edir. 
    /*tr.find("#lblWorkDescription").text(WorkDescription);
    tr.find("#lblNumber").text(Number);
    tr.find("#lblDepartment").text(txtDepartment);*/

    tr.find('.edit-mode, .display-mode').toggle();

    var _DATA =
    {
        "UserName": UserName,
        "DisplayName": DisplayName
    };

    $.ajax({
        url: '/Admin/SaveUserData/',
        data: JSON.stringify(_DATA),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data == "Success")
                DialogAlert('success', "Məlumat yadda saxlanildi"); //Bunun kodu jQuery_PopUpScipts.js
                //alert('success', "Məlumat yadda saxlanildi");
            else
                DialogAlert('danger', "Uğursuz nəticələndi");
                //alert('danger', "Uğursuz nəticələndi");
        }
    });

});