﻿using System;
using System.Collections.Generic;
using System.Linq;
//
//using System.Data.Entity;
using System.Data;
using PhoneBook_LDAP.Models;
using System.DirectoryServices;
using IntranetMvcApplication.Models;
using System.Text.RegularExpressions;
using System.Data.Entity;
//using System.Text.RegularExpressions;

namespace IntranetMvcApplication.Util
{
    public class LDAP_Tool
    {
        public List<Users> GetADUsers()
        {
            try
            {
                List<Users> lstADUsers = new List<Users>();
                string DomainPath = "LDAP://DC=xalqbank,DC=az";
                DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                search.Filter = "(&(objectClass=user)(objectCategory=person)(userAccountControl=512))";
                search.PropertiesToLoad.Add("samaccountname");
                search.PropertiesToLoad.Add("mail");
                search.PropertiesToLoad.Add("usergroup");
                search.PropertiesToLoad.Add("displayname");
                search.PropertiesToLoad.Add("telephoneNumber");
                search.PropertiesToLoad.Add("description");
                search.PropertiesToLoad.Add("distinguishedname");
                search.PropertiesToLoad.Add("dn");


                //first name
                SearchResult result;
                SearchResultCollection resultCol = search.FindAll();
                if (resultCol != null)
                {
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                        string UserNameEmailString = string.Empty;
                        result = resultCol[counter];
                        if (result.Properties.Contains("samaccountname") &&
                             /*    result.Properties.Contains("mail") &&
                            result.Properties.Contains("displayname") &&*/
                            result.Properties.Contains("telephoneNumber") )
                        {
                            Users objSurveyUsers = new Users();
                            //objSurveyUsers.Email = (String)result.Properties["mail"][0];
                            //objSurveyUsers.UserName = (String)result.Properties["samaccountname"][0];
                            //objSurveyUsers.DisplayName = (String)result.Properties["displayname"][0];
                            //objSurveyUsers.Phone = (String)result.Properties["telephoneNumber"][0];    

                            if (result.Properties.Contains("samaccountname"))
                                objSurveyUsers.UserName = (String)result.Properties["samaccountname"][0];
                            else
                                objSurveyUsers.Email = "";

                            if (result.Properties.Contains("mail"))
                                objSurveyUsers.Email = (String)result.Properties["mail"][0];
                            else
                                objSurveyUsers.Email = "";


                            if (result.Properties.Contains("displayname"))
                                objSurveyUsers.DisplayName = (String)result.Properties["displayname"][0];
                            else
                                objSurveyUsers.DisplayName = "";


                            if (result.Properties.Contains("telephoneNumber"))
                                objSurveyUsers.Phone = (String)result.Properties["telephoneNumber"][0];
                            else
                                objSurveyUsers.Phone = "";

                            if (result.Properties.Contains("description"))
                                objSurveyUsers.Description = (String)result.Properties["description"][0];
                            else
                                objSurveyUsers.Description = "";

                            if (result.Properties.Contains("distinguishedname"))
                                objSurveyUsers.Distinguishedname = (String)result.Properties["distinguishedname"][0];
                            else
                                objSurveyUsers.Distinguishedname = "";


                            if (result.Properties.Contains("distinguishedname"))
                            {
                                objSurveyUsers.Distinguishedname = (String)result.Properties["distinguishedname"][0];
                                objSurveyUsers.DN = SetDN_from_distinguishedname(objSurveyUsers.Distinguishedname);
                            }
                            else
                            {
                                objSurveyUsers.Distinguishedname = "";
                                objSurveyUsers.DN = "";
                            }


                            lstADUsers.Add(objSurveyUsers);
                        }
                    }
                }
                return lstADUsers;
            }
            catch (Exception ex)
            {
                return null;
            } 
        }


        public List<Users> GetADUsers_ByNameOrPhone(String NameOrPhone)
        {
            try
            {
                List<Users> lstADUsers = new List<Users>();
                string DomainPath = "LDAP://DC=xalqbank,DC=az";
                DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                search.Filter = "(&(objectClass=user)(objectCategory=person)(userAccountControl=512)(|(displayname="+ NameOrPhone + ")(telephoneNumber=" + NameOrPhone + ")))";
                search.PropertiesToLoad.Add("samaccountname");
                search.PropertiesToLoad.Add("mail");
                search.PropertiesToLoad.Add("usergroup");
                search.PropertiesToLoad.Add("displayname");
                search.PropertiesToLoad.Add("telephoneNumber");
                search.PropertiesToLoad.Add("description");
                search.PropertiesToLoad.Add("distinguishedname");
                //search.PropertiesToLoad.Add("dn");

                //first name
                SearchResult result;
                SearchResultCollection resultCol = search.FindAll();
                if (resultCol != null)
                {
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                        string UserNameEmailString = string.Empty;
                        result = resultCol[counter];
                        if (result.Properties.Contains("samaccountname") &&
                                 /*result.Properties.Contains("mail") &&
                            result.Properties.Contains("displayname") &&*/
                            result.Properties.Contains("telephoneNumber") )
                        {
                            Users objSurveyUsers = new Users();
                            //objSurveyUsers.Email = (String)result.Properties["mail"][0];
                            //objSurveyUsers.UserName = (String)result.Properties["samaccountname"][0];
                            //objSurveyUsers.DisplayName = (String)result.Properties["displayname"][0];
                            //objSurveyUsers.Phone = (String)result.Properties["telephoneNumber"][0];    

                            if (result.Properties.Contains("samaccountname"))
                                objSurveyUsers.UserName = (String)result.Properties["samaccountname"][0];
                            else
                                objSurveyUsers.Email = "";

                            if (result.Properties.Contains("mail"))
                                objSurveyUsers.Email = (String)result.Properties["mail"][0];
                            else
                                objSurveyUsers.Email = "";

                            if (result.Properties.Contains("displayname"))
                                objSurveyUsers.DisplayName = (String)result.Properties["displayname"][0];
                            else
                                objSurveyUsers.DisplayName = "";

                            if (result.Properties.Contains("telephoneNumber"))
                                objSurveyUsers.Phone = (String)result.Properties["telephoneNumber"][0];
                            else
                                objSurveyUsers.Phone = "";

                            if (result.Properties.Contains("description"))
                                objSurveyUsers.Description = (String)result.Properties["description"][0];
                            else
                                objSurveyUsers.Description = "";

                            if (result.Properties.Contains("distinguishedname"))
                            {
                                objSurveyUsers.Distinguishedname = (String)result.Properties["distinguishedname"][0];
                                objSurveyUsers.DN = SetDN_from_distinguishedname(objSurveyUsers.Distinguishedname);
                            }
                            else
                            {
                                objSurveyUsers.Distinguishedname = "";
                                objSurveyUsers.DN = "";
                            }

                            /*if (result.Properties.Contains("dn"))
                                objSurveyUsers.DN = (String)result.Properties["dn"][0];
                            else
                                objSurveyUsers.DN = "";*/



                            lstADUsers.Add(objSurveyUsers);
                        }
                    }
                }
                return lstADUsers;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        private String SetDN_from_distinguishedname(String distinguishedname_)
        {
            //Regex regex = new Regex("World", RegexOptions.IgnoreCase);
            String res="";


            string pattern = @"OU=(.*?),OU";
            Regex regex = new Regex(pattern);

            Match match = regex.Match(distinguishedname_);

            // 
            while (match.Success)
            {
                //Console.WriteLine(match.Groups[1].Value);
                res = match.Groups[1].Value;
                return res;

                //match = match.NextMatch();
            }


            return res;
        }

        public List<OUnit> GetMemberOf()
        {
            List<OUnit> ADGroupS = new List<OUnit>();

            string Department = "IT Billing";

            //string DomainPath = @"LDAP://DC=xalqbank,DC=az";
            //string DomainPath = @"LDAP://OU=APPLICATION,DC=xalqbank,DC=az";
            string DomainPath = @"LDAP://OU=Informasiya texnogiyalari Idaresi,OU=Xalq Bank Central Office,DC=xalqbank,DC=az";
            
            DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);
            DirectorySearcher search = new DirectorySearcher(searchRoot);
            //search.Filter = string.Format("(&(objectClass=user)(department={0}))", Department);
            search.Filter = "(&(objectClass=user)(objectCategory=person))";
            search.SearchScope = SearchScope.Subtree;

            SearchResultCollection results = search.FindAll();

            for (int i = 0; i < results.Count; i++)
            {
                OUnit objGroups = new OUnit();
                objGroups.Name = results[i].Path;
                ADGroupS.Add(objGroups);
            }
            return ADGroupS;
        }

        public List<OUnit> ADGetMemberOf()
        {
            List<OUnit> ADGroupS = new List<OUnit>();

            string DomainPath = "LDAP://DC=xalqbank,DC=az";
            DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);
            DirectorySearcher search = new DirectorySearcher(searchRoot);
            //search.Filter = "(&(objectClass=user)(objectCategory=person))";
            search.Filter = "(&(objectClass=group))";
            search.SearchScope = SearchScope.Subtree;

            SearchResultCollection results = search.FindAll();

            for (int i = 0; i < results.Count; i++)
            {
                OUnit objGroups = new OUnit();
                //DirectoryEntry de = results[i].GetDirectoryEntry();
                objGroups.Name = results[i].Path;
                ADGroupS.Add(objGroups);
                //return de.Name.ToString();
            }
            return ADGroupS;
        }

        //OU-larin listesini geri gonderir
        public List<OUnit> GetADGroups()
        {
            List<OUnit> lstADGroups = new List<OUnit>();
            string DomainPath = "LDAP://DC=xalqbank,DC=az";
            DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);
            DirectorySearcher search = new DirectorySearcher(searchRoot);
            search.Filter = "(&(objectClass=organizationalUnit))";
            search.SearchScope = SearchScope.Subtree;
            search.PropertiesToLoad.Add("distinguishedName");
            SearchResult result;
            SearchResultCollection resultCol = search.FindAll();

            for (int counter = 0; counter < resultCol.Count; counter++)
            {
                string UserNameEmailString = string.Empty;
                result = resultCol[counter];

                if (result.Properties.Contains("distinguishedName"))
                {
                    OUnit objSurveyGr = new OUnit();
                    objSurveyGr.Name = result.Properties["distinguishedName"][0].ToString().Replace(",DC=xalqbank,DC=az", "");
                    
                    lstADGroups.Add(objSurveyGr);
                }
            }

            search.Dispose();

            return lstADGroups;
        }

        //OU-larin listesini goturur ve eyni zamanda bu listeni bazaya yazir
        /*public List<OUnit> GetOU_DB_Write()
        {
            List<OUnit> lstADGroups = new List<OUnit>();

            //*** LDAP-dan OU melumatlari goturen hisse
            //Melumatlari grp_-a yazir
            List<Grp_> grp_ = new List<Grp_>();

            string DomainPath = "LDAP://DC=xalqbank,DC=az";
            DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);
            DirectorySearcher search = new DirectorySearcher(searchRoot);
            search.Filter = "(&(objectClass=organizationalUnit))";
            search.SearchScope = SearchScope.Subtree;
            search.PropertiesToLoad.Add("distinguishedName");
            SearchResult result;
            SearchResultCollection resultCol = search.FindAll();

            for (int counter = 0; counter < resultCol.Count; counter++)
            {
                result = resultCol[counter];

                if (result.Properties.Contains("distinguishedName"))
                {
                    Grp_ grp_buffer = new Grp_();
                    grp_buffer.OU.Name = result.Properties["distinguishedName"][0].ToString().Replace(",DC=xalqbank,DC=az", "");
                    grp_buffer.OU_Say = CountStringOccurrences(grp_buffer.OU.Name, "OU=");
                    if (grp_buffer.OU_Say > 1)
                        grp_buffer.OU_Parent = grp_buffer.OU.Name.Substring(grp_buffer.OU.Name.IndexOf("OU=", 2));

                    grp_.Add(grp_buffer);
                }
            }

            search.Dispose();
            //*** END of LDAP-dan OU melumatlari goturen hisse 

            //OU-larin siralanmasi
            grp_ = grp_.OrderBy(x => x.OU_Say).ThenBy(y => y.OU_Parent).ToList();

            //*** OU-larin bazaya yazilmasi
            DirectoryContext db = new DirectoryContext();
            int ou_lvl = 1;
            OUnit ParOU = null;

            for (int c = 0; c < grp_.Count(); c++)
            {
                if (ou_lvl < grp_[c].OU_Say)
                    ou_lvl = grp_[c].OU_Say;

                if (grp_[c].OU_Parent != null)
                    ParOU = grp_.First(x => x.OU.Name == grp_[c].OU_Parent).OU;

                OUnit OU_data = new OUnit { Name = grp_[c].OU.Name, ParentOU = ParOU };
                db.OUnit.Add(OU_data);
            }

            db.SaveChanges();
            //***END of OU-larin bazaya yazilmasi

            //return ucun melumati hazirlayir
            for (int i = 0; i < grp_.Count; i++)
                lstADGroups.Add(grp_[i].OU);

            return lstADGroups;
        }*/


        public bool Users_DB_Write()
        {
            LDAPContext ld = new LDAPContext();
            IQueryable<Users> usrs = GetUsers();

            foreach (Users usr_ in usrs)
            {
                var un = usr_;

                if (ld.Users.Any(e => e.UserName == usr_.UserName))
                {
                    ld.Users.Attach(usr_);
                    ld.Entry(usr_).State = EntityState.Modified;
                    ld.Entry(usr_).Property(x => x.DisplayName).IsModified = false; //bu field-i update etmir

                    /*ld.Entry(usr_).Property(x => x.Phone).IsModified = true;
                    ld.Entry(usr_).Property(x => x.Email).IsModified = true;
                    ld.Entry(usr_).Property(x => x.Description).IsModified = true;
                    ld.Entry(usr_).Property(x => x.Distinguishedname).IsModified = true;
                    ld.Entry(usr_).Property(x => x.DN).IsModified = true;
                    ld.Entry(usr_).Property(x => x.Groups).IsModified = true;*/
                }
                else
                {
                    ld.Entry(usr_).State = EntityState.Added;
                }

                ld.SaveChanges();
            }

            return false;
        }

        private IQueryable<Users> GetUsers()
        {
            //LDAP_Tool alo = new LDAP_Tool();

            List<Users> usrs = GetADUsers();

            IQueryable<Users> usrs_ = Enumerable.Empty<Users>().AsQueryable();

            try
            {
                usrs_ = usrs.AsQueryable();
            }
            catch (System.ArgumentNullException ex)
            {
            }

            return usrs_;
        }

        //string occurence-lerin sayini geri qaytarir
        public int CountStringOccurrences(string text, string pattern)
        {
            // Loop through all instances of the string 'text'.
            int count = 0;
            int i = 0;
            while ((i = text.IndexOf(pattern, i)) != -1)
            {
                i += pattern.Length;
                count++;
            }
            return count;
        }

    }

    //Group modeline elave OU_say ve OU_Parent siralama ve parent-i tapmaq ucun
    class Grp_
    {
        public OUnit OU = new OUnit();
        public int OU_Say;
        public string OU_Parent;
    }
}