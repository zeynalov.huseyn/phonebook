﻿using PhoneBook_updt.Models;
using System.Data.Entity;
using System.Linq;

namespace PhoneBook_LDAP.Models
{
    public class LDAPContext : DbContext
    {

        public LDAPContext()
            : base("OracleLDAPContext")
        {
        }

        static LDAPContext()
        {
            Database.SetInitializer<LDAPContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("XBAPP_PB");

            modelBuilder.Configurations.Add(new Users_Map());
        }

        public DbSet<Users> Users { get; set; }
        //public DbSet<OUnit> OUnit { get; set; }
        //public DbSet<> Books { get; set; }
        //public DbSet<Purchase> Purchases { get; set; }


        //Filterlere gore secilmiw sifariwler
        public IQueryable<Users> GetUserByNameAndPhone(string SeacrhFor = "", int Page = 1, int rowPerPage = 50)
        { 
            LDAPContext db = new LDAPContext();

            //var InUsrs = db.Users.Where(c => (c.UserName.Contains(SeacrhFor)) || (c.Phone.Contains(SeacrhFor))).Select(p => p.UserName);

            IQueryable<Users> usrs = db.Users.Where(c => (c.DisplayName.ToLower().Contains(SeacrhFor.ToLower())) || (c.Phone.Contains(SeacrhFor))).
                OrderBy(p => p.DisplayName).Skip(rowPerPage * (Page - 1)).Take(rowPerPage);

            return usrs;
        }


    }

    /*      grant create table to xbapp_pb ;

grant insert on xbapp_pb.pb_users to xbapp_pb;
grant update on xbapp_pb.pb_users to xbapp_pb;

    ALTER USER xbapp_pb quota unlimited on users;
     */
}