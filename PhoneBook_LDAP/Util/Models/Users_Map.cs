﻿using PhoneBook_LDAP.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace PhoneBook_updt.Models
{
    class Users_Map : EntityTypeConfiguration<Users>
    {
        public Users_Map()
        {
            // Primary Key
            this.HasKey(t => t.UserName);

            // Properties
            this.Property(t => t.UserName)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

           /* this.Property(t => t.MNEMO)
                .HasMaxLength(30);*/

            // Table & Column Mappings
            this.ToTable("PB_USERS");

            this.Property(t => t.UserName).HasColumnName("USERNAME");
            this.Property(t => t.DisplayName).HasColumnName("DISPLAYNAME");
            this.Property(t => t.isMapped).HasColumnName("ISMAPPED");
            this.Property(t => t.Groups).HasColumnName("GROUPS");
            this.Property(t => t.Email).HasColumnName("EMAIL");
            this.Property(t => t.Phone).HasColumnName("PHONE");
            this.Property(t => t.Description).HasColumnName("DESCRIPTION");
            this.Property(t => t.Distinguishedname).HasColumnName("DISTINGUISHEDNAME");
            this.Property(t => t.DN).HasColumnName("DN");


            /*this.Property(t => t.TTT)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);*/
        }


        /*
         * 
            grant create table to xbapp_pb ;

            grant insert on xbapp_pb.pb_users to xbapp_pb;
            grant update on xbapp_pb.pb_users to xbapp_pb;

        [Key]
        public string UserName { get; set; }

        [Display(Name = "A.A.S.", AutoGenerateFilter = true)]
        public string DisplayName { get; set; }

        public bool isMapped { get; set; }
        public string Groups { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Description { get; set; }
        public string Distinguishedname { get; set; }
        public string DN { get; set; }   

    
     CREATE TABLE pb_users (
        UserName varchar2(200) NOT NULL,
        DisplayName varchar2(200) NOT NULL,
        isMapped NUMBER(1) DEFAULT 0,
        Groups varchar2(1000),
        Email varchar2(200),
        Phone varchar2(50),
        Description varchar2(1000),
        Distinguishedname varchar2(1000),
        DN varchar2(200)
        );

        ALTER TABLE pb_users ADD (
          CONSTRAINT pb_users_pk PRIMARY KEY (UserName));
     */
    }
}
