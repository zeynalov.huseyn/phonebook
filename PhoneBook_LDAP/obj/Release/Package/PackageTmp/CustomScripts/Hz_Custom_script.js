﻿$(function () {
    //***************
    //Tarix secilende datepicker
    $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });


    //***************
    //Page-in sehifeleri basilanda
    $('.gridPage').on('click', function (e) {
        e.preventDefault();

        $('#Page').val($(this).attr("alt"));
        var form = $('form#FrmFilter');

        form.submit();
    });

    //***************
    //Filter Form-daki button basilanda
    $('#BtnFrmFilter').on('click', function (e) {
        e.preventDefault();

        $('#Page').val(1);
        var form = $('form#FrmFilter');

        form.submit();
    });

});