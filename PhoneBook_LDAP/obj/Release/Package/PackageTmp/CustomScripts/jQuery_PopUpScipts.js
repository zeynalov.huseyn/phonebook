﻿//Require
//<link href="~/Content/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
//<script src="~/Scripts/jquery-ui-1.12.0.js"></script>

//Popup Dialog
$("#dialog").dialog({
    autoOpen: false,
    show: {
        effect: "blind",
        duration: 500
    },
    hide: {
        effect: "explode",
        duration: 500
    }
});

$("#opener").on("click", function () {
    $("#dialog").dialog("open");
});


function DialogAlert(type, message) {
    //Notifications evezine alert-area da var. Istesen ora yaz
    $("#dialog").dialog("open");
    $("#dialog").prepend($("<div class='alert alert-message alert-" + type + "'><strong> " + message + " </strong></div>"));
    $(".alert-message").delay(4000).slideUp("slow", function () { $(this).remove(); $("#dialog").dialog("close"); });
    
}



//PhoneBook insert dialog box save
$('#dialogInsertPB_Ok').on('click', function () {
    var dia = $(this).closest('#dialogInsertPB');

    var FullName = dia.find("#insFullName").val();  //edit-mode Yeni melumati goturur
    var WorkDescription = dia.find("#insWorkDescription").val();
    var Number = dia.find("#insNumber").val();
    var Department = dia.find("#insDepartment").val();
    var txtDepartment = dia.find("#insDepartment").find('option:selected').text();;

    var _DATA =
    {
        "FullName": FullName,
        "Number": Number,
        "WorkDescription": WorkDescription,
        "DepartmentId": Department
    };    


    $.ajax({
        url: '/Admin/InsertPhoneData/',
        data: JSON.stringify(_DATA),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',

        success: function (data) {
            if (data == "Success")
                DialogAlert('success', "Məlumat yadda saxlanildi"); //Bunun kodu jQuery_PopUpScipts.js
                //alert('success', "Məlumat yadda saxlanildi");
            else
                DialogAlert('danger', "Uğursuz nəticələndi");
            //alert('danger', "Uğursuz nəticələndi");
        },
        error: function (jqXHR,textStatus,errorThrown) {
            //DialogAlert('danger', "Uğursuz nəticələndi");
            alert('danger', "Uğursuz nəticələndi");
            //OnUploadFail(jqXHR, textStatus, errorThrown);                
        }
    });    

});