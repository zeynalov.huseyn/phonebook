﻿using IntranetMvcApplication.Util;
using PhoneBook_updt.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PhoneBook_LDAP.Models
{
    public class LDAPContext : DbContext
    {

        public LDAPContext()
            : base("OracleLDAPContext")
        {
        }

        static LDAPContext()
        {
            Database.SetInitializer<LDAPContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("XBAPP_PB");

            modelBuilder.Configurations.Add(new Users_Map());
        }

        public DbSet<Users> Users { get; set; }
        //public DbSet<OUnit> OUnit { get; set; }
        //public DbSet<> Books { get; set; }
        //public DbSet<Purchase> Purchases { get; set; }
               
    }

    /*      grant create table to xbapp_pb ;

grant insert on xbapp_pb.pb_users to xbapp_pb;
grant update on xbapp_pb.pb_users to xbapp_pb;

    ALTER USER xbapp_pb quota unlimited on users;
     */
}