﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntranetMvcApplication.Models
{
    public class OUnit
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public OUnit ParentOU { get; set; }
    }
}