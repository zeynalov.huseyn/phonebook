﻿using System.ComponentModel.DataAnnotations;

namespace PhoneBook_LDAP.Models
{
    public class Users
    {        
        [Key]
        public string UserName { get; set; }

        [Display(Name = "A.A.S.", AutoGenerateFilter = true)]
        public virtual string DisplayName { get; set; }

        public int isMapped { get; set; }
        public virtual string Groups { get; set; }
        public virtual string Email { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Description { get; set; }
        public virtual string Distinguishedname { get; set; }
        public virtual string DN { get; set; }
    }

    /*
     CREATE TABLE pb_users (
        UserName varchar2(200) NOT NULL,
        DisplayName varchar2(200) NOT NULL,
        isMapped NUMBER(1) DEFAULT 0,
        Groups varchar2(1000),
        Email varchar2(200),
        Phone varchar2(50),
        Description varchar2(1000),
        Distinguishedname varchar2(1000),
        DN varchar2(200)
        );

        ALTER TABLE pb_users ADD (
          CONSTRAINT pb_users_pk PRIMARY KEY (UserName));
     */
}